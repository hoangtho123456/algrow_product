<!DOCTYPE html>
<html <?php language_attributes( ); ?>>
<head>
  <meta charset="utf-8">
  <title>Home | Training Wordpress</title>
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/reset.css'; ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(). '/css/style.css'; ?>">
  <!-- <link rel="stylesheet" type="text/css" href="./css/style.css"> -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="c-header">
  <div class="l-container">
    <div class="c-header__top">
      <div class="logo">
        <a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/img/logo.png' ?>"
          alt="税理士法人下平会計事務所"></a>
      </div>
      <div class="contact">
        <img src="<?php echo get_template_directory_uri().'/img/hed_tel.png' ?>" alt=""><br/>
        <img src="<?php echo get_template_directory_uri().'/img/hed_con_no.png' ?>" alt="" class="imglink">
      </div>
    </div>

    <nav class="c-nav">
      <ul>
        <?php wp_nav_menu(); ?>
      </ul>
    </nav>
  </div>
</header>