<?php get_header(); ?>
  <h1 class="my-4">
    <?php
      _e('<h2>404 NOT FOUND</h2>');
      _e('<p>The article you were looking for was not found, but maybe try looking again!</p>');
    ?>
  </h1><!-- end my-4 -->
  </div>
  <!-- Sidebar Widgets Column -->
<div class="col-md-4">
  <?php dynamic_sidebar('right-sidebar'); ?>
</div>
<?php get_footer(); ?>
