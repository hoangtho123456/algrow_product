<?php
/*Template Name: Service*/
?>

<?php get_header(); ?>

<main class="p-service">
  <div class="l-container">
    <?php
      while(have_posts()) : the_post();
        the_content();
      endwhile;
    ?>
  </div>
</main>

<?php get_footer(); ?>
