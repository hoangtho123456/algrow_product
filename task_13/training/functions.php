<?php
/*
*add theme_support(menu, thumbnail, ....) for wordpress;
*/
add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );

/*
*function remove amrgin-top: 31px !important;
*/
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

/*
*add boostrap link and file js, css
*/
function reg_scripts() {
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all' );
  wp_enqueue_script( 'Common', get_template_directory_uri() . '/js/common.js',
    array(), true);
}
add_action( 'wp_enqueue_scripts', 'reg_scripts' );

/*
*Register right sidebar
*/
function sidebar_unit() {
  register_sidebar( array(
    'name' => __('Left Hand Sidebar'),
    'id' => 'left-sidebar',
    'class' => 'c-list',
    'description' => __('Widgets in this area will be shown on the left-hand side'),
    'before_title' => '<h3 class="c-widget__title">',
    'after_title' => '</h3>',
    'before_widget' => '<div id="%1$s" class="c-widget">',
    'after_widget' => '</div><!-- widget end -->'
  ));
}
add_action('widgets_init', "sidebar_unit");


function wpse_my_widget_output_filter( $widget_output, $widget_type, $widget_id ) {
    if ( 'categories' == $widget_type ) {
      $widget_output = str_replace('<ul class="c-list">', $widget_output);
    }
    return $widget_output;
}
add_filter( 'widget_output', 'my_widget_output_filter_footer', 10, 3 );


/*
*add class css page-link to post-link(pagination)
*/
function posts_link_attributes() {
  return 'class="page-link"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

/*
*pagination
*/
if (!function_exists('pagination_nav')) {
  function pagination_nav() {
  global $wp_query;

  if ( $wp_query->max_num_pages < 2 ) {
    return;
   }
   ?>

    <ul class="pagination justify-content-center mb-4" role="navigation">
      <?php
        //if previous posts pages link existed, web will show a link name '← Older'.
        if(get_previous_posts_link('← Older')!==null) {?>
          <li class="page-item">
            <?php previous_posts_link( '← Older'); ?>
          </li><!-- end page-item -->
        <?php
        } else
        //if no previous posts pages link, web will show a link '← Older' disabled.
        {?>
          <li class="page-item disabled">
            <?php echo '<a class="page-link" href="" title="">← Older</a>'; ?>
          </li><!-- end page-item -->
          <?php
        }
        //if next posts pages link existed, web will show a link name 'Newer →'.
        if(get_next_posts_link( 'Newer →')!==null) {?>
          <li class="page-item">
            <?php next_posts_link( 'Newer →'); ?>
          </li><!-- end page-item -->
        <?php
        } else
        //if next posts pages link existed, web will show a link name 'Newer →' disabled.
        {?>
          <li class="page-item disabled">
            <?php echo '<a class="page-link" href="" title="">Newer →</a>'; ?>
          </li><!-- end page-item -->
          <?php
        }
      ?>
    </ul>
    <!-- ---------------------------------------- -->
   <?php
  //-------------------------------------------------
  }
  //-------------------------------------------------
}

if ( function_exists("acf_add_options_page") ) {
  acf_add_options_page();
  acf_add_options_sub_page("Header");
  acf_add_options_sub_page("Footer");

  acf_add_options_page(array(
    "page_title" => "Theme Options",
    "menu_title" => 'Theme Options',
    "menu_slug" => 'theme-options',
    'capability' => 'edit_posts',
    'parent_slug' => '',
    'position' => false,
    'icon_url' => false,
    'redirect' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Header",
    "menu_title" => 'Header',
    "menu_slug" => 'theme-options-header',
    'capability' => 'edit_posts',
    'parent_slug' => 'theme-options',
    'position' => false,
    'icon_url' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Footer",
    "menu_title" => 'Footer',
    "menu_slug" => 'theme-options-footer',
    'capability' => 'edit_posts',
    'parent_slug' => 'theme-options',
    'position' => false,
    'icon_url' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Post Settings",
    "menu_title" => "Post Settings",
    "menu_slug" => "post-settings",
    'capability' => 'edit_posts',
    'parent_slug' => 'edit.php',
    'position' => false,
    'icon_url' => false
  ));
}

/**
@add link category in list post
**/
if ( !function_exists("addLinkCategory_dangtho")) {
  function addLinkCategory_dangtho($catName) {
    $catID = get_cat_ID($catName);
    $catLink = get_category_link($catID);

    echo $catLink;
  }
}

/**
@pagination for category
**/
if (!function_exists("pagination_cat_dangtho")) {
  function pagination_cat_dangtho() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer

    $html = paginate_links( array(
      'paged' =>  ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'total' => $wp_query->max_num_pages,
      'show_all' => true,
      'prev_next' => true,
      'prev_text' => __("«"),
      'next_text' => __("»")
    ));
    //set your additional decorative elements
    //mimics the default for paginate_links()
    $pretext = '«';
    $posttext = '»';
    //assuming this set of links goes at bottom of page
    $pre_deco = '<a class="prev btn-pagi--mr1 page-numbers" href="" >' . $pretext . '</a>';
    $post_deco = '<a class="next btn-pagi--ml1 page-numbers" href="" >' . $posttext . '</a>';
     //key variable
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    //add decorative non-link to first page
    if ( 1 === $paged) {
      $html = $pre_deco . $html;
    }
    //add decorative non-link to last page
    if ( $wp_query->max_num_pages == $paged ) {
      $html = $html . $post_deco;
    }
    echo $html;
  }
}
