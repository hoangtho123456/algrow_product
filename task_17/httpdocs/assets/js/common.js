
/* スマートロールオーバー */
function smartRollover(){if(document.getElementsByTagName)for(var b=document.getElementsByTagName("img"),a=0;a<b.length;a++)b[a].getAttribute("src").match("_out.")&&(b[a].onmouseover=function(){this.setAttribute("src",this.getAttribute("src").replace("_out.","_over."))},b[a].onmouseout=function(){this.setAttribute("src",this.getAttribute("src").replace("_over.","_out."))})}window.addEventListener?window.addEventListener("load",smartRollover,!1):window.attachEvent&&window.attachEvent("onload",smartRollover);

/*--------------------------------------------------------------------------*
 *
 *  SmoothScroll JavaScript Library V2
 *
 *  MIT-style license.
 *
 *  2007-2011 Kazuma Nishihata
 *  http://www.to-r.net
 *
 *--------------------------------------------------------------------------*/
new function(){function f(a){function d(a,c,b){setTimeout(function(){"up"==b&&a>=c?(a=a-(a-c)/20-1,window.scrollTo(0,a),d(a,c,b)):"down"==b&&a<=c?(a=a+(c-a)/20+1,window.scrollTo(0,a),d(a,c,b)):scrollTo(0,c)},10)}if(b.getElementById(a.href.replace(/.*\#/,""))){a=b.getElementById(a.href.replace(/.*\#/,"")).offsetTop;var c=b.documentElement.scrollHeight,e=window.innerHeight||b.documentElement.clientHeight;c-e<a&&(a=c-e);c=window.pageYOffset||b.documentElement.scrollTop||b.body.scrollTop||0;d(c,a,a<c?
"up":"down")}}var g=/noSmooth/,b=document;(function(a,d,c){try{a.addEventListener(d,c,!1)}catch(b){a.attachEvent("on"+d,function(){c.apply(a,arguments)})}})(window,"load",function(){for(var a=$("a[href*='#']").not(".noscrl"),b=0,c=a.length;b<c;b++)g.test(a[b].getAttribute("data-tor-smoothScroll"))||a[b].href.replace(/\#[a-zA-Z0-9_]+/,"")!=location.href.replace(/\#[a-zA-Z0-9_]+/,"")||(a[b].onclick=function(){f(this);return!1})})};
// noscrlというクラスを付けることでスムーススクロールの対象外にできます。（<a href="#header" class="noscrl">hoge</a>）

//===================================================
// SP menu
//===================================================
$(function() {
  $(".c-menuSP-js").on('click', function() {
    $(this).toggleClass("is-open");
    $('.c-menulistSP').slideToggle(400);
  });
});

//===================================================
// SP scrolltop
//===================================================
$(".c-f-btn2-js").click(function() {
  $("html, body").animate({ scrollTop: 0 }, 1000);
  return false;
});

//===================================================
// navigation menu SP
//===================================================
//1.slide up sub menu
(function slideUpSubmenu() {
  $(".openSub_js").each(function(index){
  	if (!$(this).has(".active_js").length) {
      $(this).next(".c-sub__nav_js").hide();
  	}
  });
})();

//2.Make effect for icon and show sub menu 
//when click a link contain sub menu
(function () {
  $(".c-menulistSP .openSub_js").click(function(e) {
    e.preventDefault();

    let iconDown = $(this).children(".fa-chevron-down");
    iconDown.toggleClass("offSub");
    
    $(this).toggleClass("active_js");
    $(this).next(".c-sub__nav_js").slideToggle(300);
  });
})();

//===================================================
// show/ hide search form in SP creen
//===================================================
$(".btn-search1_js").click(function (e) {
  $(".form-search1_js").slideToggle(200);
});

//===================================================
// show sub navigation on the main menu
//===================================================
$(".navigation_sub_js").parent().hover(function() {
  var submenu = $(this).children(".navigation_sub_js");

  if ($(submenu).is(":hidden")) {
    $(submenu).slideDown(200);
  } else {
    $(submenu).slideUp(200);
  }
});

//===================================================
// show tab at footer when the page is being showed 
//on SP screen
//===================================================
//1.slide up tab in footer on Smart phone
(function slideUpTabFooter() {
  hideTabFooter();

  $(window).resize(function() {
    if($(window).width() <= 767) {
      hideTabFooter();
    } else {
      $(".f-tab_js").each(function(index){
        var tab = $(this).next(".f-content_js");
        $(tab).show();
      });
    }
  });
})();

//2.toggle tab
$(".f-tab_js").click(function() {
  var icon = $(this).find(".footer-plus_js");

  if (!$(icon).is(":hidden")) {
    var tab = $(this).next(".f-content_js");
    $(tab).slideToggle(200);
    icon.toggleClass("is-open");
  }
});

/*----------------------------------------
* hide tab in footer
----------------------------------------*/
function hideTabFooter() {
  $(".f-tab_js").each(function(index){
    var icon = $(this).find(".footer-plus_js");

    if (!$(icon).is(":hidden") && !$(icon).hasClass("is-open")) {
      var tab = $(this).next(".f-content_js");
      $(tab).hide();
    }
  });
}

//===================================================
// slider(using library slick.js)
//===================================================
$(document).ready(function(){
  //slider in the top page
  $('.c-slider_js').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fas fa-chevron-right"></i></button>'
  });

  //slider at p-top1
  $('.c-slider1_js').slick({
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2100,
    arrows: true,
    prevArrow: '<button type="button" class="c-prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="c-next"><i class="fas fa-chevron-right"></i></button>'
  });

  //slider at the Product's component
  $('.c-slider2_js').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    variableWidth: true,
    arrows: true,
    prevArrow: '<button type="button" class="c-prev"><i class="fas fa-chevron-left"></i></button>',
    nextArrow: '<button type="button" class="c-next"><i class="fas fa-chevron-right"></i></button>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          infinite: true,
          slidesToShow: 2,
          variableWidth: false
        }
      },
      {
        breakpoint: 550,
        settings: {
          infinite: true,
          slidesToShow: 1,
          variableWidth: false
        }
      }
    ]
  });
});
