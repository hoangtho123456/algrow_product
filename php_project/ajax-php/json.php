<?php
//connect DB
$conn = mysqli_connect('localhost', 'root', '', 'demo_ajax') or die('Cannot connect to DB');

//Get List member
$query = mysqli_query($conn, 'SELECT * FROM member');

$result = array();

if(mysqli_num_rows($query) > 0) {
  while($row = mysqli_fetch_array($query)) {
    $result[] = array(
      'username' => $row['username'],
      'email' => $row['email']
    );
  }
}

die (json_encode($result));
?>