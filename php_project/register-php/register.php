<?php
  include('server.php');
?>
<!DOCTYPE html>
<html>
<head>
  <title>User registration system using PHP and MySQL</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
    <h2>Register</h2>
  </div>

  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="POST" onsubmit="return submit_form()" accept-charset="utf-8">
    <!-- display validation errors here -->
    <?php
      include("errors.php");
    ?>
    <div class="err">
    </div>
    <div class="input-group">
      <label>Username</label>
      <input type="text" name="username" value="<?php echo $username; ?>">
    </div>
    <div class="input-group">
      <label>Email</label>
      <input type="email" name="email" value="<?php echo $email; ?>">
    </div>
    <div class="input-group">
      <label>Password</label>
      <input type="password" name="password_1">
    </div>
    <div class="input-group">
      <label>Confirm password</label>
      <input type="password" name="password_2">
    </div>
    <div class="input-group">
      <button type="submit" name="register" class="btn">Register</button>
    </div>
    <p>Already a number? <a href="login.php" title="">Sign in</a></p>
  </form>

  <script src="js/js.js" type="text/javascript">
  </script>
</body>
</html>
