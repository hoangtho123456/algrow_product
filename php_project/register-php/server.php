<?php
  session_start();

  $username = "";
  $email = "";
  $errors = array();

  //connect to the database
  $db = mysqli_connect('localhost', 'root', '', 'registration');

  //if the register button is clicked
  if (isset($_POST["register"])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

    //ensure that form fields are filled properly
    if(empty($username)) {
      array_push($errors, "Username is required!"); //add error to errors array
    }
    if(empty($email)) {
      array_push($errors, "Email is required!"); //add error to errors array
    }
    if(empty($password_1)) {
      array_push($errors, "Password is required!"); //add error to errors array
    }
    if ($password_1 != $password_2) {
      array_push($errors, "The two password do not match!");
    }

    //check user is existed or not in database
    $sql = "SELECT * FROM users WHERE username='$username'";
    $result = mysqli_query($db, $sql);

    if (mysqli_num_rows($result) == 1) {
      array_push($errors, 'username was existed!');
    }

    //if there are no errors, save user to database
    if (count($errors) == 0) {
      $password = md5($password_1); // encrypt password before storing in database(security)

      $sql = "INSERT INTO users(username, email, password)
          VALUES('$username', '$email', '$password')";
      mysqli_query($db, $sql);
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in!";
      header("location: index.php");
    }
  }

  //log user in from login page
  if(isset($_POST['login'])) {
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    //ensure that form fields are filled properly
    if(empty($username)) {
      array_push($errors, "Username is required!"); //add error to errors array
    }
    if(empty($password)) {
      array_push($errors, "Password is required!"); //add error to errors array
    }

    if(count($errors) == 0) {
      $password = md5($password); //encrypt password before comparing with that from db
      $query = "SELECT * FROM users WHERE username=? AND password=?";

      try {
        if ($stmt = mysqli_prepare($db, $query)) {
          $param_name = $username;
          $param_pw = $password;

          //Bind variables to the prepared statement as parameters
          mysqli_stmt_bind_param($stmt, "ss", $param_name, $param_pw);

          if(mysqli_stmt_execute($stmt)){
            $result = mysqli_stmt_get_result($stmt);

            //Check username and password are existed or not.
            if(mysqli_num_rows($result) == 1) {
              $_SESSION["username"] = $username;
              $_SESSION["success"] = "You are now logged in!";
              header("location: index.php");
              exit();
            } else {
              array_push($errors, "username or password was not correct!");
            }
          }

          mysqli_stmt_close($stmt);
          mysqli_close($db);
        }
      } catch(Exception $e) {
        echo 'Caught exception: ' . $e->getMessage() . "\n";
      }

    }
  }

  //logout
  if(isset($_GET['logout'])) {
    session_destroy();
    header('location: login.php');
  }
?>
