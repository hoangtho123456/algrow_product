<?php
  include('server.php');
?>
<!DOCTYPE html>
<html>
<head>
  <title>User registration system using PHP and MySQL</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
    <h2>Login</h2>
  </div>

  <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
    method="POST" accept-charset="utf-8">
    <!-- display validation errors here -->
    <?php
      include("errors.php");
    ?>
    <div class="input-group">
      <label>Username</label>
      <input type="text" name="username" value="" placeholder="username">
    </div>
    <div class="input-group">
      <label>Password</label>
      <input type="password" name="password" value="" placeholder="password">
    </div>
    <div class="input-group">
      <button type="submit" name="login" class="btn">Login</button>
    </div>
    <p>Not yet a number? <a href="register.php" title="">Sign up</a></p>
  </form>

</body>
</html>
