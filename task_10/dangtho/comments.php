<?php
if(post_password_required()) {
  return;
}
?>

<div id="comments" class="comments-area">
  <?php
  // You can start editing here -- including this comment!
  if ( have_comments() ) : ?>
    <?php
      comment_form(array(
        'title_reply_before' =>'<h5 class="card-header">',
        'title_reply'=>'Leave a Comment',
        'title_reply_after' => '</h5>',
        'label_submit' => '',
        'comment_field' =>
          '<div class="card-body">
            <div class="form-group">
              <textarea class="form-control" rows="3" style="margin-top: 0px; margin-bottom: 0px; height: 89px;" id="comment" name="comment" aria-required="true"></textarea>
            </div>
            <p class="form-submit c-inlineblock">
              <input class="btn btn-primary" name="submit" type="submit" id="submit" class="submit" value="Submit">
              <input type="hidden" name="comment_post_ID" value="2" id="comment_post_ID">
              <input type="hidden" name="comment_parent" id="comment_parent" value="0">
            </p>
          </div>'
      ));

      $fields =  array(
        'author' =>
          '<p class="comment-form-author"><label for="author">' . __( 'Name', 'domainreference' ) .
          ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
          '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
          '" size="30"' . $aria_req . ' /></p>',

        'email' =>
          '<p class="comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) .
          ( $req ? '<span class="required">*</span>' : '' ) . '</label>' .
          '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
          '" size="30"' . $aria_req . ' /></p>',

        'url' =>''
      );
      apply_filters( 'comment_form_default_fields', $fields )
    ?>
    <div class="comment-list">
      <?php
        wp_list_comments( array(
          'avatar_size' => 50,
          'style'       => 'ul',
          'short_ping'  => true
        ) );
      ?>
    </div>

    <?php the_comments_pagination(array());

  endif; // Check for have_comments().

  // If comments are closed and there are comments, let's leave a little note, shall we?
  if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

    <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyseventeen' ); ?></p>
  <?php
  endif;
  ?>

</div><!-- #comments -->
