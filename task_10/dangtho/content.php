<?php
/**
 * The default template for displaying content
 * Used for both single and index/archive/search.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <header class="entry-header">
    <?php
      if ( is_single() ) :
        the_title( '<h1 class="mt-4">', '</h1>' );
      else :
        the_title( sprintf( '<h1 class="mt-4">', esc_url( get_permalink() ) ), '</h1>' );
      endif;
    ?>
    <p class="lead">by <a href="#">
      <?php the_author_posts_link(); ?></a></p>
    <hr>
    <p>Posted on <?php the_time('F j, Y g:i A'); ?></p>
    <hr>
  </header><!-- .entry-header -->

  <?php
    // Post thumbnail.
    the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
  ?>
  <hr>
  <div class="entry-content">
    <?php
      /* translators: %s: Name of current post */
      the_content( sprintf(
        __( 'Continue reading %s' ),
        the_title( '<span class="screen-reader-text">', '</span>', false )
      ) );

      wp_link_pages( array(
        'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'Title!' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
        'pagelink'    => '<span class="screen-reader-text">' . __( 'Page' ) . ' </span>%',
        'separator'   => '<span class="screen-reader-text">, </span>',
      ) );
    ?>
  </div><!-- .entry-content -->
  <?php
    // Author bio.
    if ( is_single() && get_the_author_meta( 'description' ) ) :
      get_template_part( 'author-bio' );
    endif;
  ?>

</article><!-- #post-## -->
<hr>
