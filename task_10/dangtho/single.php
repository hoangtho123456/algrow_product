<?php get_header(); ?>
  <?php if(have_posts()): ?>
    <?php while(have_posts()):the_post(); ?>
    <h1 class="mt-4"><?php the_title(); ?></h1>
    <p class="lead">by <a href="#">
      <?php the_author_posts_link(); ?></a></p>
    <hr>
    <p>Posted on <?php the_time('F j, Y g:i A'); ?></p>
    <hr>

    <?php
      // Post thumbnail.
      the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
    ?>

    <hr>
    <?php the_content(); ?>

    <hr>
    <?php
      // If comments are open or we have at least one comment, load up the comment template.
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
    ?>
    <?php endwhile; ?>
  <?php else: ?>
    <h1>Cand find post!</h1>
  <?php endif; ?>
<?php get_footer(); ?>
