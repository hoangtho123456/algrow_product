<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--interview">
  <div class="c-banner1 c-banner1__interview">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/interview/INTERVIEW.png' ;?>" alt="INTERVIEW">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>インタビュー一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-interview1">
      <div class="l-container">
        <article class="p-interview1__title1">
          <h2>就活に役立つコラムと<br>
          週刊愛媛経済レポート連載の経営者インタビューを紹介！</h2>
        </article>

        <!-- <?php //echo do_shortcode('[multitab_ajax per_page="9"]'); ?> -->
        <?php
        $terms  = get_terms('category');
        if(count($terms)): ?>
        <div id="container-async" class="p-interview1__listbtn1">
        <div class="btn__tab1">
          <a href="<?php echo get_home_url() . '/interview'; ?>" data-filter="<?php echo 'all' ?>" data-term="all-terms" data-page="1">すべて</a>
        </div>

        <?php foreach ($terms as $term) : ?>
        <div class="btn__tab1 <?php if ($term->term_id == $status['active']) :?> active<?php endif; ?>">
          <a href="<?php echo get_term_link( $term, $term->taxonomy ); ?>" data-filter="<?php echo $term->taxonomy; ?>" data-term="<?php echo $term->slug ?>" data-page="1"><?php echo $term->name; ?></a>
        </div>
        <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <div class="p-interview1__tab1">
          <div class="p-interview1__list1">
            <?php if(have_posts()): ?>
            <?php while(have_posts()) : the_post(); ?>
              <div class="c-card1">
                <div class="c-card1__box1">
                  <div class="c-card1__img1">
                    <?php
                      // Post thumbnail.
                      the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
                    ?>
                  </div>
                  <a class="small-title1"
                  href="<?php addLinkCategory_dangtho(get_the_category( $id )[0]->name); ?>">
                    <p><?php echo get_the_category( $id )[0]->name; ?></p>
                  </a>
                </div>

                <div class="c-card1__text1">
                  <?php
                  $message = get_field('message');
                  if($message):
                  ?>
                  <a class="card1__title1 card1__link1" href="<?php the_permalink(); ?>">
                    <?php echo $message['title1']; ?>
                    <i class="img-icon1"><img src="<?php echo get_template_directory_uri() . 
                    '/img/icon1.png'; ?>" alt="icon1"></i>
                  </a>

                  <p><?php echo $message['text1']; ?></p>
                  <?php endif; ?>
                </div>
              </div>
            <?php endwhile;?>
          </div>
        </div><!--end list1-->
        <div class="c-pagination">
          <?php
            $html = paginate_links( array(
              'paged' => ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
              'total' => $query->max_num_pages,
              'show_all' => false,
              'prev_next' => true,
              'prev_text' => __("PREV"),
              'next_text' => __("NEXT")
            ));
            echo $html;
            wp_reset_postdata();
            ?>
        </div>
        <?php endif; ?>
      </div><!--end l-container-->
    </div><!--end p-company1-->
  </div><!-- end l-container-->
</main><!-- end l-main -->

<?php get_footer(); ?>
