<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--interview">
  <div class="c-banner1 c-banner1__interview">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/interview/INTERVIEW.png' ;?>" alt="INTERVIEW">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">

  <?php if(have_posts()): ?>
    <?php while(have_posts()) : the_post(); ?>
    <div class="l-container">
      <div class="c-breadcrumb">
        <div class="l-container">
          <a href="<?php echo get_home_url(); ?>">ホーム</a>
          <a href="<?php echo get_home_url() . '/interview'; ?>">インタビュー一覧</a>
          <span><?php the_title(); ?></span>
        </div>
      </div><!--end breadcrumb-->
    </div><!-- end l-container-->

    <section class="p-insingle1">
      <div class="p-insingle1__inner">
        <div class="p-insingle1__box1">
          <?php
            // Post thumbnail.
            the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
          ?>
        </div>
        <div class="p-insingle1__box2">
          <div class="insingle1__content1">
            <div class="content1__row1">
              <span class="datepost"><?php echo get_the_date("Y.m.d"); ?></span>
              <div class="cate1">
                <h3><?php echo get_the_category( $id )[0]->name; ?></h3>
              </div>
            </div>

            <div class="content1__row2">
              <?php
              $message = get_field('message');
              if($message):
              ?>
              <div class="custom__mess1">
                <h2><?php echo $message['title1']; ?></h2>
              </div>

              <div class="custom__mess2">
                <p><?php echo $message['text1']; ?></p>
              </div>
              <?php endif; ?>

              <div class="custom__name">
                <h3><?php echo get_field('your_name'); ?></h3>
              </div>

              <div class="custom__profile">
              <?php
              $profile = get_field('profile');
              if($profile):
              ?>
              <p class="pro__address"><?php echo $profile['address']; ?></p>
              <p class="pro_email">E-mail : <?php echo $profile['email']; ?></p>
              <?php endif; ?>
              </div>
            </div>
          </div>
        </div><!--end p-insingle1__box2-->
      </div>
    </section><!--end p-insingle1-->

    <section class="p-insingle2">
      <div class="p-insingle2__inner">
        <?php the_content();?>

        <section class="p-insingle2__title1">
          <h3>この記事をシェア</h3>
          <div class="title1__box1">
            <div class="box1__btn1 box1__btn1--bg1">
              <a href="#">FACEBOOK</a>
            </div>

            <div class="box1__btn1 box1__btn1--bg2">
              <a href="#">TWITTER</a>
            </div>

            <div class="box1__btn1 box1__btn1--bg3">
              <a href="#">LINE</a>
            </div>
          </div>
        </section>

        <div class="p-insingle2__btn1">
          <div class="l-btn1">
            <div class="c-btn1 c-btn1--prev">
              <a href="<?php echo get_home_url() . '/interview'; ?>">ニュース一覧はこちら</a>
            </div>
          </div>
        </div><!--end p-insingle1__btn1-->
      </div><!--end p-insingle2__inner-->
    </section><!--end p-insingle2-->
    <?php endwhile; wp_reset_postdata();?>
  <?php else: ?>
    <?php _e('Sorry'); ?>
  <?php endif;?>
</main><!-- end l-main -->

<?php get_footer(); ?>
