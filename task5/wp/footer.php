  <footer class="c-footer">
    <div class="c-footer__top btn-top_js">
      <img class="showPC" src="<?php echo get_template_directory_uri() . '/img/pagetop.png'; ?>" alt="pagetop.png">
      <img class="showSP--inline" src="<?php echo get_template_directory_uri() . '/img/Scroll-To-top.png'; ?>" alt="Scroll-To-top.png">
    </div>

    <div class="c-footer__box1">
      <div class="l-container">
        <div class="footer__inner1">

          <div class="footer__flexbox">
            <div class="footer__box2">
              <div class="box2__title1">
                <p>掲載企業に関するご質問や資料請求などお気軽にご相談ください。</p>
              </div>

              <div class="box2__tel">
                <h2><span class="sm-text1">TEL.</span> 089-947-1411</h2>
              </div>

              <div class="box2__title2">
                <div class="title2__inner">
                  <h3><span class="sm-text1">受付時間</span>9:00〜17:00（平日のみ）</h3>
                </div>
              </div>
            </div><!--end footer__box2-->

            <div class="footer__box3">
              <div class="c-footer__btn1">
                <a href="">お問い合わせはこちら</a>
              </div>

              <div class="c-footer__btn1">
                <a href="">掲載・取材依頼の企業様へ</a>
              </div>
            </div><!--end footer__box3-->
          </div>

          <div class="footer__box1">
            <div class="box1__logo">
              <img class="showPC" src="<?php echo get_template_directory_uri() . '/img/f_logo.png'; ?>" alt="f_logo.png">
              <img class="showSP--inline" src="<?php echo get_template_directory_uri() . '/img/sp-f_logo.png'; ?>" alt="sp-f_logo">
            </div>

            <div class="box1__text1">
              <h3>【運営】株式会社愛媛経済レポート</h3>
              <p>〒790-0011<br class="showSP--inline">　愛媛県松山市千舟町4丁目5-4　松山千舟454ビル5F</p>
            </div>
          </div>
        </div><!--end footer__inner1-->
      </div><!--end l-container-->
    </div> <!-- end c-footer__box1 -->

    <div class="c-copyright">
      <p>©2018 Ehime Keizai Report Inc,</p>
    </div>
  </footer> <!-- end c-footer -->

  <script>
    $(".btn-top_js").click(function(e) {
      $("html, body").animate({ scrollTop: 0 }, 1000);
      return false;
    });
  </script>
  <?php wp_footer(); ?>
</body>
</html>
