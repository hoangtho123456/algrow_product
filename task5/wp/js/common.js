/* スマートロールオーバー */
function smartRollover(){if(document.getElementsByTagName)for(var b=document.getElementsByTagName("img"),a=0;a<b.length;a++)b[a].getAttribute("src").match("_out.")&&(b[a].onmouseover=function(){this.setAttribute("src",this.getAttribute("src").replace("_out.","_over."))},b[a].onmouseout=function(){this.setAttribute("src",this.getAttribute("src").replace("_over.","_out."))})}window.addEventListener?window.addEventListener("load",smartRollover,!1):window.attachEvent&&window.attachEvent("onload",smartRollover);

/*--------------------------------------------------------------------------*
 *
 *  SmoothScroll JavaScript Library V2
 *
 *  MIT-style license.
 *
 *  2007-2011 Kazuma Nishihata
 *  http://www.to-r.net
 *
 *--------------------------------------------------------------------------*/
new function(){function f(a){function d(a,c,b){setTimeout(function(){"up"==b&&a>=c?(a=a-(a-c)/20-1,window.scrollTo(0,a),d(a,c,b)):"down"==b&&a<=c?(a=a+(c-a)/20+1,window.scrollTo(0,a),d(a,c,b)):scrollTo(0,c)},10)}if(b.getElementById(a.href.replace(/.*\#/,""))){a=b.getElementById(a.href.replace(/.*\#/,"")).offsetTop;var c=b.documentElement.scrollHeight,e=window.innerHeight||b.documentElement.clientHeight;c-e<a&&(a=c-e);c=window.pageYOffset||b.documentElement.scrollTop||b.body.scrollTop||0;d(c,a,a<c?
"up":"down")}}var g=/noSmooth/,b=document;(function(a,d,c){try{a.addEventListener(d,c,!1)}catch(b){a.attachEvent("on"+d,function(){c.apply(a,arguments)})}})(window,"load",function(){for(var a=$("a[href*='#']").not(".noscrl"),b=0,c=a.length;b<c;b++)g.test(a[b].getAttribute("data-tor-smoothScroll"))||a[b].href.replace(/\#[a-zA-Z0-9_]+/,"")!=location.href.replace(/\#[a-zA-Z0-9_]+/,"")||(a[b].onclick=function(){f(this);return!1})})};
// noscrlというクラスを付けることでスムーススクロールの対象外にできます。（<a href="#header" class="noscrl">hoge</a>）

//===================================================
// SP menu
//===================================================
$(function toggleMenuFollowScreenSize() {
  $(".closeMenu_js, .c-menuSP-js").on('click', function() {
    $(".c-menuSP-js").toggleClass("is-open");
    $('.c-menu1_js').slideToggle(400);

    $btnMenu = $(".c-menuSP-js");

    //if screen's size > breakpoint, menu will be showed
    //if screen's size <= breakpoint and menu was not show
    //on the screen, menu will be hidden
    $(window).resize(function() {
      if($(window).width() > 767) {
        $('.c-menu1_js').css("display", "block");
      } else if(!$btnMenu.hasClass("is-open")) {
        $('.c-menu1_js').css("display", "none");
      }
    });
  });
});

//===================================================
// SP scrolltop
//===================================================
$(".btn-top_js").click(function() {
  $("html, body").animate({ scrollTop: 0 }, 1000);
  return false;
});

//===================================================
// slider(using library slick.js)
//===================================================
$(document).ready(function() {
  //slider effection in page Index
  $(".c-slider1_js").slick({
    infinite: true,
    autoplay: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 3000,
    arrows: false
  });

  //slides in Top1 component
  $(".p-top1__list_js").slick({
    infinite: true,
    dots: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    centerMode: true,
    variableWidth: true,
    arrows: true,
    prevArrow: '<button class="p-top1__prev"><img src="/wp_task10/wp-content/uploads/2019/04/arrow3.png" alt="arrow3.png"></button>',
    nextArrow: '<button class="p-top1__next"><img src="/wp_task10/wp-content/uploads/2019/04/arrow2.png" alt="arrow2.png"></button>',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          infinite: true,
          arrows: false
        }
      }
    ]
  });

  //slides in Top1 component
  $(".single-sider1_js").slick({
    infinite: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          infinite: true,
        }
      }
    ]
  });
});

