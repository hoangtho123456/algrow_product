<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--about">
  <div class="c-banner1 c-banner1__about">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/about/ABout.png'; ?>" alt="ABout.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <section class="p-about1">
    <div class="l-container">
      <div class="c-breadcrumb">
        <div class="l-container">
          <a href="index.html">ホーム</a>
          <span>愛媛シゴト図鑑とは</span>
        </div>
      </div><!--end breadcrumb-->

      <div class="p-about1__box1">
        <div class="p-about1__title1">
          <h3>愛媛シゴト図鑑のコンセプト</h3>
          <h2>仕事選びの不安を無くしたい</h2>
        </div><!--end p-about1__title1-->

        <div class="p-about1__img1">
          <img src="<?php echo get_template_directory_uri() . 
          '/img/about/concept_head.png'; ?>" alt="concept_head.png">
        </div>
      </div><!--end p-about1__box1-->
    </div><!--end l-container-->
  </section><!--end p-about1-->

  <section class="p-about2">
    <div class="p-about2__box1 p-about2--mb1">
      <div class="box1__content1"></div>

      <div class="box1__content2">
        <div class="content2__text1">
          <h2>愛媛シゴト図鑑とは</h2>
          <p>愛媛シゴト図鑑は、「仕事選びの不安を無くす」をコンセプトに
          「自分がやりたいことが分からない」「どの企業に応募しようか
          迷う」「自分の人生と向き合って仕事を探したい」そんな方の一人
          ひとりが自分に合った働き方ができるよう応援しています。<br>
          企業の中身が見えなくて不安を感じ、自分が入社した後の想像がつ
          かない学生さん。実際に働かれている方のインタビューを通して、
          会社の雰囲気、働くことの喜びを感じとってください。会社のいい
          ところだけでなく、大変なところや働く人の思い、葛藤なども紹
          介しています。そのため、必ず職場を訪ねて、本音を引き出す独自
          の取材を行っています。</p>
        </div>
      </div>
    </div>

    <div class="p-about2__box1 p-about2__box1--reverse">
      <div class="box1__content1"></div>

      <div class="box1__content2">
        <div class="content2__text1">
          <h2>わたしたちについて</h2>
          <p>これまで45年に渡り、愛媛の経済情報を専門に取材し、
            週刊愛媛経済レポートを発行してきました。その中で、
            愛媛にあるたくさんの素晴らしい企業の皆様にお会いしました。
            わたしたちは愛媛には社員を幸せにしている会社が多くあり、
            そして働くことは辛いばかりじゃなく、わたしたちの人生の価値
            を高めてくれるものと感じています。県内企業を見続けてきた記
            者ならではの視点で、今日もいろいろな職場を訪ねて、お話
            を聞いています。</p>
        </div>
      </div>
    </div>
  </section><!--end p-about2-->

  <section class="p-about3">
    <div class="l-container">
      <div class="c-title2">
        <h2>運営会社</h2>
      </div>

      <div class="p-about3__table1">
        <div class="c-table1">
          <table>
            <tbody>
              <tr>
                <th>社名</th>
                <td>株式会社　愛媛経済レポート</td>
              </tr>
              <tr>
                <th>設立日</th>
                <td>昭和48年9月6日</td>
              </tr>
              <tr>
                <th>資本金</th>
                <td>1000万円</td>
              </tr>
              <tr>
                <th>決算期</th>
                <td>8月</td>
              </tr>
              <tr>
                <th>本社所在地</th>
                <td>
                  <p>愛媛県松山市千舟町4丁目5-4　松山千舟454ビル5F<br>
                  TEL：089-947-1411　 FAX：089-947-1895</p>
                </td>
              </tr>
              <tr>
                <th>事業内容</th>
                <td>週間愛媛経済レポート、愛媛の会社年鑑、 えひめ業界地図等の発行</td>
              </tr>
              <tr>
                <th>沿革</th>
                <td>
                  <p>昭和48年11月1日旬刊紙（月3回）で発行<br>
                  同49年2月20日第三種郵便物許可<br>
                  同56年9月1日週刊紙に移行（年間50回発行）</p>
                </td>
              </tr>
              <tr>
                <th>HP</th>
                <td><a class="tb-link1 u-text__green" href="#">http://www.example.co.jp/</a></td>
              </tr>
              <tr>
                <th>Mail</th>
                <td><a class="tb-link1 u-text__green" href="#">example@gmail.com</a></td>
              </tr>
            </tbody>
          </table>
        </div><!--end c-table1-->
      </div><!--end p-about3__table1-->
    </div>
  </section>
</main><!-- end l-main -->

<?php get_footer(); ?>