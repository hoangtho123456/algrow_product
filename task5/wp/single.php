<?php get_header(); ?>
    <div class="l-header__main l-header__main--news">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--news">
  <div class="c-banner1 c-banner1__news">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner1__img1">
          <img src="<?php echo get_template_directory_uri()
           . '/img/news/news.png'; ?>" alt="news.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">

  <?php if(have_posts()): ?>
  <?php while(have_posts()) : the_post(); ?>
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <a href="<?php echo get_home_url() . '/news'; ?>">ニュース一覧</a><!-- do later -->
        <span><?php the_title(); ?></span>
      </div>
    </div>

    <section class="p-news-single1">
      <div class="p-news-single1__title1">
        <h4><?php echo get_the_date("Y.m.d"); ?></h4>
        <h2><?php the_title(); ?></h2>
      </div>

      <div class="p-news-single1__content1">
        <?php the_content(); ?>
      </div>

      <div class="p-news-single1__btn1">
        <div class="l-btn1">
          <div class="c-btn1 c-btn1--prev">
            <a href="<?php echo get_home_url() . "/news"; ?>">ニュース一覧はこちら</a>
          </div>
        </div>
      </div>
    </section>

  <?php endwhile; ?>
  <?php else: ?>
    <h1>Cand find post!</h1>
  <?php endif; ?>

  </div><!-- end l-container-->
</main>

<?php get_footer(); ?>
