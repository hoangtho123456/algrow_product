<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--interview">
  <div class="c-banner1 c-banner1__interview">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner__img1">
          <img src="<?php echo get_template_directory_uri() .
           '/img/interview/INTERVIEW.png' ;?>" alt="INTERVIEW">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>インタビュー一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-interview1">
      <div class="l-container">
        <article class="p-interview1__title1">
          <h2>就活に役立つコラムと<br>
          週刊愛媛経済レポート連載の経営者インタビューを紹介！</h2>
        </article>

        <?php echo do_shortcode('[multitab_ajax per_page="9"]'); ?>
      </div><!--end l-container-->
    </div><!--end p-company1-->
  </div><!-- end l-container-->
</main><!-- end l-main -->

<?php get_footer(); ?>
