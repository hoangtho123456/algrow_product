<?php
/*
*add theme_support(menu, thumbnail, ....) for wordpress;
*/
include 'loadpost_ajax/loadposts_ajax.php';

add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );

/*
 * Enable support for custom logo.
 *
 * @since Twenty Fifteen 1.5
 */
add_theme_support('custom-logo', array(
  'height'      => 248,
  'width'       => 248,
  'flex-height' => true,
));

/*
*function remove margin-top: 31px !important;
*/
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

/*
*add boostrap link and file js, css
*/
function reg_scripts() {
  wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all' );
  wp_enqueue_script( 'Common', get_template_directory_uri() . '/js/common.js',
    array(), true);
}
add_action( 'wp_enqueue_scripts', 'reg_scripts' );

function add_ajax_to_js() {
  wp_enqueue_script( 'ajax', get_template_directory_uri() . '/js/ajax_loadpost.js',['jquery'], null, true);
  wp_localize_script( 'ajax', 'bobz', array(
    'nonce' => wp_create_nonce( 'bobz' ),
    'ajax_url' => admin_url( 'admin-ajax.php' )
  ));
}
add_action( 'wp_enqueue_scripts', 'add_ajax_to_js' );

/*
@Custom field
*/
//I just added '!' below at Match 1 2019, if false, please delete it!^^
if ( function_exists("acf_add_options_page") ) {
  acf_add_options_page();

  acf_add_options_page(array(
    "page_title" => "Theme Options",
    "menu_title" => 'Theme Options',
    "menu_slug" => 'theme-options',
    'capability' => 'edit_posts',
    'parent_slug' => '',
    'position' => false,
    'icon_url' => false,
    'redirect' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Header",
    "menu_title" => 'Header',
    "menu_slug" => 'theme-options-header',
    'capability' => 'edit_posts',
    'parent_slug' => 'theme-options',
    'position' => false,
    'icon_url' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Footer",
    "menu_title" => 'Footer',
    "menu_slug" => 'theme-options-footer',
    'capability' => 'edit_posts',
    'parent_slug' => 'theme-options',
    'position' => false,
    'icon_url' => false
  ));

  acf_add_options_page(array(
    "page_title" => "Post Settings",
    "menu_title" => "Post Settings",
    "menu_slug" => "post-settings",
    'capability' => 'edit_posts',
    'parent_slug' => 'edit.php',
    'position' => false,
    'icon_url' => false
  ));
}

/**
@pagination for category
**/
if (!function_exists("pagination_cat_dangtho")) {
  function pagination_cat_dangtho($query) {
    global $wp_query;
    $big = 999999999; // need an unlikely integer

    if ($query == NULL) {
      $query = $wp_query;
    } else $query = $query;

    $html = paginate_links( array(
      'paged' =>  ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
      'total' => $query->max_num_pages,
      'show_all' => false,
      "end_size" => 4,
      'prev_next' => true,
      'prev_text' => __("PREV"),
      'next_text' => __("NEXT")
    ));
    echo $html;
  }
}

/*
*Custom Post type(success)
*/
function createCustomPostType_Company() {
  //$label uses for contain text which relative to the name
  //of post type
  $label = array(
    'name' => 'Company',
    'singular_name' => 'company'
  );

  $array = array(
    'labels' => $label,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    //taxonomys be admit to separate contents
    'taxonomies' => array('category', 'post_tag'),
    'hierarchical' => false, //allow decentralization, if false => like post, true => like page
    'public' => true, //activated post type
    'show_ui' => true, //show manager box like Post/Page
    'show_in_menu' => true, //show on Admin menu(left)
    'show_in_nav_menus' => true, //show on Appearance -> Menus
    'show_in_admin_bar' => true, //show on Admin bar(black)
    'menu_position' => 5, //order show in menu(left)
    'menu_icon' => '',
    'can_export' => true, //Can export content by Tools -> Export
    'has_archive' => true, //allow store (month, date, year)
    'exclude_from_search' => false, //delete from search result
    'publicly_queryable' => true, //Show parameters on query, must to set true.
    'capability_type' => 'post' //
  );

  register_post_type("Company", $array);
}
add_action('init', 'createCustomPostType_Company');

//show custom post type in main layout
add_filter('pre_get_posts', 'getCustomPostType_Company');
function getCustomPostType_Company($query) {
  if(is_home() && $query -> is_main_query()) {
    $query-> set('post_type', array('post', 'Company'));
  }
  return $query;
}

/*
*Custom Post type(publish)
*/
function createCustomPostType_Interview() {
  //$label uses for contain text which relative to the name
  //of post type
  $label = array(
    'name' => 'Interview',
    'singular_name' => 'Interview'
  );

  $array = array(
    'labels' => $label,
    'supports' => array(
      'title',
      'editor',
      'excerpt',
      'author',
      'thumbnail',
      'comments',
      'trackbacks',
      'revisions',
      'custom-fields'
    ),
    //taxonomys be admit to separate contents
    'taxonomies' => array('category','post_tag'),
    'hierarchical' => false, //allow decentralization, if false => like post, true => like page
    'public' => true, //activated post type
    'show_ui' => true, //show manager box like Post/Page
    'show_in_menu' => true, //show on Admin menu(left)
    'show_in_nav_menus' => true, //show on Appearance -> Menus
    'show_in_admin_bar' => true, //show on Admin bar(black)
    'menu_position' => 5, //order show in menu(left)
    'menu_icon' => '',
    'can_export' => true, //Can export content by Tools -> Export
    'has_archive' => true, //allow store (month, date, year)
    'exclude_from_search' => false, //delete from search result
    'publicly_queryable' => true, //Show parameters on query, must to set true.
    'capability_type' => 'post' //
  );

  register_post_type("interview", $array);
}
add_action('init', 'createCustomPostType_Interview');

//show custom post type in main layout
add_filter('pre_get_posts', 'getCustomPostType_Interview');
function getCustomPostType_Interview($query) {
  if(is_home() && $query -> is_main_query()) {
    $query-> set('post_type', array('post', 'Interview'));
  }
  return $query;
}


// Register Custom Taxonomy
function custom_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Taxonomy', 'text_domain' ),
    'all_items'                  => __( 'All Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add New Item', 'text_domain' ),
    'edit_item'                  => __( 'Edit Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Search Items', 'text_domain' ),
    'not_found'                  => __( 'Not Found', 'text_domain' ),
    'no_terms'                   => __( 'No items', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'taxonomy', array( 'interview' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );


/**
@add link category in list post
**/
if ( !function_exists("addLinkCategory_dangtho")) {
  function addLinkCategory_dangtho($catName) {
    $catID = get_cat_ID($catName);
    $catLink = get_category_link($catID);
    //----------------------------------------
    echo $catLink;
  }
}

/*
@action: get url link from custom post type (id: option)
*/
if(!function_exists("getBtnLinkCustomPost")) {
  function getBtnLinkCustomPost($linkname, $option) {
    $link = get_field($linkname, $option);
    if ($link):?>
      <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>">
        <?php echo $link['title']; ?>
      </a>
    <?php endif;
  }
}

