<?php get_header(); ?>
    <div class="c-header__top">
      <div class="showPC">
        <div class="link1">
          <a class="icon1" href="#">
            掲載・取材依頼の企業様へ
            <img src="<?php echo get_template_directory_uri() . '/img/icon.png'; ?>" alt="icon.png">
          </a>
        </div>
      </div><!--end showPC-->
    </div><!--end-->

    <div class="l-header__main">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual">
  <div class="showPC">
    <div class="c-slider1 c-slider1_js">
      <?php
      $images = get_field('slider', 'options');
      $size = 'full';
      if ($images): ?>
        <?php foreach($images as $image): ?>
          <div class="c-slider1__img1">
            <?php echo wp_get_attachment_image($image['ID'], $size); ?>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div><!-- end c-slider-->
  </div>

  <div class="showSP">
    <div class="c-slider1 c-slider1_js">
      <?php
      $images = get_field('slider_sp', 'options');
      $size = 'full';
      if ($images): ?>
        <?php foreach($images as $image): ?>
          <div class="c-slider1__img1">
            <?php echo wp_get_attachment_image($image['ID'], $size); ?>
          </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div><!-- end c-slider-->
  </div>

  <div class="c-banner1 c-banner1__top">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner1__img1">
          <img class="showPC" src="<?php echo get_template_directory_uri()
           . '/img/mv_catch.png'; ?>" alt="mv_catch.png">
          <img class="showSP--inline" src="<?php echo get_template_directory_uri()
           . '/img/sp-mv_catch.png'; ?>" alt="sp-mv_catch.png">
        </div>

        <div class="banner1__text1">
          <p>2020年卒学生向けの企業紹介サイト</p>
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <section class="p-top1">
    <div class="p-top1__title1">
      <div class="title1">
        <h2><span class="title1__sm1">Pick Up</span>
        新着ピックアップ</h2>
      </div>
    </div>

    <div class="p-top1__box1">
      <div class="p-top1__list p-top1__list_js">
        <?php $query = new WP_Query(array(
        'post_type'=>'interview',
        'posts_per_page'=> 7,
        'post_status'=>array('future', 'publish'),
        'paged' => get_query_var('paged'))); ?>

        <?php if($query->have_posts()): ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
        <div class="c-card1">
          <div class="c-card1__box1">
            <div class="c-card1__img1">
              <?php
              // Post thumbnail.
              the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
              ?>
            </div>
            <a class="small-title1" 
            href="<?php addLinkCategory_dangtho(get_the_category( $id )[0]->name); ?>">
              <p><?php echo get_the_category( $id )[0]->name; ?></p>
            </a>
          </div>

          <div class="c-card1__text1">
            <?php
            $message = get_field('message');
            if($message):
            ?>
            <a class="card1__link1" href="<?php the_permalink(); ?>">
              <?php echo $message['title1']; ?>
            </a>
            <p><?php echo $message['text1']; ?></p>
            <?php endif; ?>
          </div>
        </div>
        <?php endwhile; wp_reset_postdata();?>
        <?php else: ?>
          <?php _e('Sorry'); ?>
        <?php endif;?>
      </div><!--end p-top1__list-->
    </div><!--end p-top1__list1-->
  </section><!--end p-top1-->

  <section class="p-top2">
    <div class="l-container">
      <div class="p-top2__title1">
        <div class="title1">
          <h2><span class="title1__sm">Corporate Information</span>
          企業一覧</h2>
        </div>
      </div><!--end p-top2__title1-->

      <div class="p-top2__text1">
        <p>愛媛で働きたい。<br class="showSP">
          けれど、どんな会社があるか分からない。</p>
        <p>愛媛シゴト図鑑では、愛媛に本社がある企業のみご紹介！</p>
      </div>

      <div class="p-top2__list1">
        <?php $query = new WP_Query(array(
        'post_type'=>'company',
        'posts_per_page'=> 6,
        'post_status'=>array('future', 'publish'),
        'paged' => get_query_var('paged'))); ?>

        <?php if($query->have_posts()): ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
          <a class="list1__link1" href="<?php the_permalink(); ?>">
             <div class="list1__card1">
              <div class="card1__box1">
                <div class="card1__img1">
                  <?php
                    // Post thumbnail.
                    the_post_thumbnail('full', array('class' => 'img-fluid rounded'));
                    ?>
                </div>
              </div>

              <div class="card1__box2">
                <h3><?php the_title(); ?></h3>
                <span class="small-text1"><?php echo get_field('location'); ?></span>

                <div class="card1__text1">
                  <p><?php echo get_field('message'); ?></p>
                </div>

                <?php
                $post_tags = get_the_tags();
                if ( $post_tags ):?>
                  <span class="tag1"><?php echo $post_tags[0]->name; ?></span>
                <?php endif; ?>
              </div>
            </div><!--end card1-->
          </a>
        <?php endwhile; wp_reset_postdata();?>
        <?php else: ?>
          <?php _e('Sorry'); ?>
        <?php endif;?>
      </div><!--end p-top2__list1-->

      <div class="l-btn1">
        <div class="c-btn1 p-mt1">
          <a href="<?php echo get_home_url() . '/company'; ?>">企業一覧はこちら</a>
        </div>
      </div>
    </div><!--end l-container-->
  </section><!--end p-top2-->

  <section class="p-top3">
    <div class="p-top3__inner">
      <div class="showPC">
        <div class="book-bg">
          <img src="<?php echo get_template_directory_uri() . '/img/book1.jpg'; ?>" alt="book1.jpg">
        </div>
      </div>

      <div class="l-container">
        <section class="p-top3__box1">
          <div class="l-title1 l-title1--mb1">
            <div class="c-title1">
              <h3 class="l-mb1">News</h3>
              <h2>新着ニュース</h2>
            </div>
          </div>
          <?php $query = new WP_Query(array('post_type'=>'post', 
          'post_status'=>'publish', 'posts_per_page' => 5, 'paged' => get_query_var( 'paged' )));
          if($query->have_posts()): ?>
          <ul class="c-listpost">
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <li class="c-listpost__item">
              <span class="datepost"><?php echo get_the_date(" Y.m.d "); ?></span>
              <h3 class="c-listpost__title">
                <a href="<?php the_permalink();?>" title=""><?php the_title(); ?></a>
              </h3>
            </li>
            <?php endwhile; ?>
          </ul><!--end c-listpost-->
          <?php else: ?>
            <?php _e('Sorry'); ?>
          <?php endif;?>

          <div class="l-btn1">
            <div class="c-btn1">
              <a href="<?php echo get_home_url() . "/news"; ?>">ニュース一覧はこちら</a>
            </div>
          </div>
        </section><!--end p-top3__box1-->
      </div><!--end l-container-->
    </div><!--end p-top3__inner-->
  </section><!--end p-top3-->

  <section class="p-top4">
    <div class="p-top4__inner">
      <div class="p-top4__box1 po-absolute">
        <div class="box1__img1">
          <img class="showPC" src="<?php echo get_template_directory_uri() . '/img/center.png'; ?>" alt="center.png">
          <img class="showSP--inline" src="<?php echo get_template_directory_uri() . '/img/h2_about_sp.png'; ?>" alt="h2_about_sp.png">
        </div>

        <div class="box1__text1">
          <p>愛媛シゴト図鑑は、これから就職を目指す若者と、<br>
          愛媛の企業をつなぐ役割を担っています。</p>
        </div>
      </div>

      <div class="p-top4__box2">
        <div class="box2__img1 po-absolute">
          <img src="<?php echo get_template_directory_uri() . '/img/img11.jpg'; ?>" alt="img11.jpg">
        </div>

        <div class="box2__img2 po-absolute">
          <img src="<?php echo get_template_directory_uri() . '/img/img21.jpg'; ?>" alt="img21.jpg">
        </div>
      </div>

      <div class="p-top4__btn1">
        <div class="btn1__inner">
          <div class="c-btn2">
            <a href="">愛媛シゴト図鑑とは</a>
          </div>
        </div>
      </div><!--end p-top4__btn1-->
    </div><!--end p-top4__box1-->
  </section><!--end p-top4-->
</main><!-- end l-main -->

<?php get_footer(); ?>
