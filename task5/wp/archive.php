<?php get_header(); ?>
  <div class="l-navigation l-navigation__news">
    <div class="c-navigation">
      <div class="l-container">
        <?php get_template_part("content", "menu"); ?>
      </div>
    </div><!--end c-navigation-->
  </div><!--end l-navigation-->

  <div class="showSP">
    <nav class="c-menulistSP">
      <div class="c-menuSP c-menuSP-js">
        <div class="c-menuSP__1"></div>
        <div class="c-menuSP__2"></div>
        <div class="c-menuSP__3"></div>
        <div class="c-menuSP__title1">
          <p>閉じる</p>
        </div>
      </div>

      <ul class="mt1">
        <li><a href="">ホーム</a></li>
        <li><a href="">企業一覧</a></li>
        <li><a href="">愛媛シゴト図鑑とは</a></li>
        <li><a href="">インタビュー</a></li>
        <li><a href="">ニュース</a></li>
        <li><a href="">運営会社</a></li>
        <li><a href="">お問い合わせ</a></li>
      </ul>

      <div class="menulistSP__box1">
        <div class="box1__tel">
          <h2><span class="sm-text1">TEL.</span> 089-947-1411</h2>
        </div>

        <div class="box1__title1">
          <h3>受付時間9:00〜17:00（平日のみ）</h3>
        </div>
      </div>

      <div class="menulistSP__btn1">
        <div class="btn1__inner">
          <a href="#">掲載・取材のご依頼についてはこちら</a>
        </div>
      </div>

      <ul>
        <li><a href="#">プライバシーポリシー</a></li>
        <li><a href="#">利用規約</a></li>
        <li class="closeMenu closeMenu_js"><a href="#"><i class="fas fa-times"></i>閉じる</a></li>
      </ul>
    </nav><!--end c-menulistSP-->
  </div><!--show menu in SP-->

  <div class="c-header__news">
    <div class="l-banner1 l-banner1__news">
      <div class="c-banner1">
        <div class="c-banner1__news">
          <div class="l-container">
            <div class="banner__box1">
              <div class="banner__img1">
                <img src="<?php echo get_template_directory_uri() . '/img/news/news.png'; ?>" alt="news.png">
              </div>
            </div>
          </div><!--end l-container-->
        </div><!--end c-banner1__top-->
      </div><!--end c-banner1-->
    </div><!--end l-banner1-->
  </div><!-- end c-header__top -->
</header><!-- end c-header -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>ニュース一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-news1">
      <div class="p-news1__inner">
        <?php $query = new WP_Query(array('post_type'=>'post', 
          'post_status'=>'publish', 'paged' => get_query_var( 'paged' )));
          if($query->have_posts()): ?>
          <ul class="c-listpost">
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <li class="c-listpost__item">
              <span class="datepost"><?php echo get_the_date(" Y.m.d "); ?></span>
              <h3 class="c-listpost__title">
                <a href="<?php the_permalink();?>" title=""><?php the_title(); ?></a>
              </h3>
            </li>
            <?php endwhile; ?>
          </ul><!--end c-listpost-->

          <div class="p-news1__box1">
            <div class="c-pagination">
              <?php
                $html = paginate_links( array(
                  'paged' => ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
                  'total' => $query->max_num_pages,
                  'show_all' => true,
                  'prev_next' => true,
                  'prev_text' => __("PREV"),
                  'next_text' => __("NEXT")
                ));
                echo $html;
                wp_reset_postdata();
                ?>
          <!--<a class="prev">PREV</a>
              <a>1</a>
              <a>2</a>
              <a class="active">3</a>
              <a>4</a>
              <a class="next">NEXT</a> -->
            </div>
          </div>
          <?php else: ?>
            <?php _e('Sorry'); ?>
        <?php endif;?>
      </div><!--end p-news1__inner-->
    </div><!--end p-news1-->
  </div><!-- end l-container-->
</main><!-- end l-main -->
<?php get_footer(); ?>