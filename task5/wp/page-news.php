<?php get_header(); ?>
    <div class="l-header__main l-header__main--news">
      <?php get_template_part("content", "menu"); ?>
    </div><!--end-->
  </div>
</header><!-- end c-header -->

<div class="c-mainVisual c-mainVisual--news">
  <div class="c-banner1 c-banner1__news">
    <div class="l-container">
      <div class="banner__box1">
        <div class="banner1__img1">
          <img src="<?php echo get_template_directory_uri()
           . '/img/news/news.png'; ?>" alt="news.png">
        </div>
      </div>
    </div><!--end l-container-->
  </div><!--end c-banner1-->
</div><!-- end c-mainVisual -->

<main class="l-main">
  <div class="l-container">
    <div class="c-breadcrumb">
      <div class="l-container">
        <a href="<?php echo get_home_url(); ?>">ホーム</a>
        <span>ニュース一覧</span>
      </div>
    </div><!--end breadcrumb-->

    <div class="p-news1">
      <div class="p-news1__inner">
        <?php $query = new WP_Query(array('post_type'=>'post', 
          'post_status'=>'publish', 'paged' => get_query_var( 'paged' )));
          if($query->have_posts()): ?>
          <ul class="c-listpost">
            <?php while($query->have_posts()) : $query->the_post(); ?>
            <li class="c-listpost__item">
              <span class="datepost"><?php echo get_the_date(" Y.m.d "); ?></span>
              <h3 class="c-listpost__title">
                <a href="<?php the_permalink();?>" title=""><?php the_title(); ?></a>
              </h3>
            </li>
            <?php endwhile; ?>
          </ul><!--end c-listpost-->

          <div class="p-news1__box1">
            <div class="c-pagination">
              <?php
                $html = paginate_links( array(
                  'paged' => ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1,
                  'total' => $query->max_num_pages,
                  'show_all' => false,
                  'prev_next' => true,
                  'prev_text' => __("PREV"),
                  'next_text' => __("NEXT")
                ));
                echo $html;
                wp_reset_postdata();
                ?>
            </div>
          </div>
          <?php else: ?>
            <?php _e('Sorry'); ?>
        <?php endif;?>
      </div><!--end p-news1__inner-->
    </div><!--end p-news1-->
  </div><!-- end l-container-->
</main><!-- end l-main -->
<?php get_footer(); ?>