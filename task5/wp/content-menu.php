<div class="c-header__main">
  <div class="main__box1">
    <div class="logo1">
      <img class="showPC" src="<?php echo get_template_directory_uri() .
      '/img/logo.png'; ?>" alt="logo.png">
      <img class="showSP" src="<?php echo get_template_directory_uri() .
      '/img/sp-logo.png'; ?>" alt="sp-logo.png">
    </div>

    <nav class="c-menu1 c-menu1_js">
      <div class="showSP c-menuSP c-menuSP-js">
        <div class="c-menuSP__1"></div>
        <div class="c-menuSP__2"></div>
        <div class="c-menuSP__3"></div>
        <div class="c-menuSP__title1">
          <p>閉じる</p>
        </div>
      </div><!--end c-menuSP-->

      <ul class="sp-mt1">
        <li class="showSP"><a href="">ホーム</a></li>
        <li><a href="">企業一覧</a></li>
        <li><a href="">愛媛シゴト図鑑とは</a></li>
        <li><a href="">インタビュー</a></li>
        <li class="item1"><a href="">ニュース</a></li>
        <li class="showSP"><a href="">運営会社</a></li>
        <li><a href="#" class="btn1">お問い合わせ</a></li>
      </ul>

      <div class="showSP menu1__boxSP1">
        <div class="box1__tel">
          <h2><span class="sm-text1">TEL.</span> 089-947-1411</h2>
        </div>

        <div class="box1__title1">
          <h3>受付時間9:00〜17:00（平日のみ）</h3>
        </div>
      </div>

      <div class="showSP menu1__btnSP1">
        <div class="btn1__inner">
          <a href="#">掲載・取材のご依頼についてはこちら</a>
        </div>
      </div>

      <div class="showSP">
        <ul>
          <li><a href="#">プライバシーポリシー</a></li>
          <li><a href="#">利用規約</a></li>
          <li class="closeMenu closeMenu_js"><a href="#"><i class="fas fa-times"></i>閉じる</a></li>
        </ul>
      </div>
    </nav><!--end c-menu1-->

    <div class="showSP">
      <div class="listSP1">
        <a class="contact__link1" href="#">
          <div class="sp-img1">
            <img src="<?php echo get_template_directory_uri() . 
            '/img/sp-envelope.png'; ?>" alt="sp-envelope.png">
          </div>
          <div class="sp-title1">
            <h3>お問い合わせ</h3>
          </div>
        </a>

        <div class="c-menuSP c-menuSP-js">
          <div class="c-menuSP__1"></div>
          <div class="c-menuSP__2"></div>
          <div class="c-menuSP__3"></div>
          <div class="c-menuSP__title1">
            <p>メニュー</p>
          </div>
        </div><!--end c-menuSP-->
      </div><!--end-->
    </div><!--end-->
  </div><!--end-->
</div><!--end-->