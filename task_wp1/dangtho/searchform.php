<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
<div class="card-body">
  <form role="search" method="get" class="input-group" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input class="form-control" type="search" id="<?php echo $unique_id; ?>" class="search-field" placeholder="<?php echo esc_attr_x( 'Search for &hellip;', 'placeholder', 'twentyseventeen' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
    <span class="input-group-btn"><button type="submit" class="btn btn-secondary">Go!</button></span>
  </form>
</div>
