<?php get_header(); ?>
  <h1 class="my-4">Page Heading
    <small>Secondary Text</small>
  </h1><!-- end my-4 -->
  <section id="main-content">
    <div class="search-info">
      <?php
        $search_query = new WP_Query('s='.$s.'&showposts=-1');
        $search_keyword = wp_specialchars($s, 1);
        $search_count = $search_query->post_count;
        printf( __('Search results for <strong>%1$s</strong>. We found <strong>%2$s</strong> articles for you.'),
          $search_keyword, $search_count );
      ?>
    </div><!-- end search-info -->
  </section>
  <?php if(have_posts()): ?>
    <?php while(have_posts()) : the_post(); ?>
      <!-- Blog Post -->
      <div class="card mb-4">
        <?php the_post_thumbnail('full', array('class' => 'card-img-top')); ?>
        <div class="card-body">
          <h2 class="card-title"><?php the_title(); ?></h2>
          <?php the_excerpt(); ?>
          <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More &rarr;</a>
        </div> <!-- end card-body -->
        <div class="card-footer text-muted">
          Posted on <?php the_time('F jS, Y'); ?> by
          <a href="#"><?php the_author_posts_link(); ?></a>
        </div> <!-- end card-footer -->
      </div> <!-- end card -->
    <?php endwhile; ?>
  <?php else: ?>
  <?php _e('Sorry'); ?>
  <?php endif; ?>
  <?php pagination_nav(); ?>
  </div>
<!-- Sidebar Widgets Column -->
<div class="col-md-4">
  <?php dynamic_sidebar('right-sidebar'); ?>
</div>
<?php get_footer(); ?>
